VENV=.venv
BIN=$(VENV)/bin
PYTHON=$(BIN)/python3

.SILENT:

$(VENV)/bin/activate: requirements.txt
	test -d $(VENV) || python3 -m venv $(VENV)
	$(PYTHON) -m pip install --upgrade pip setuptools wheel
	$(PYTHON) -m pip install --upgrade -r requirements.txt

lint: $(VENV)/bin/activate
	$(PYTHON) -m isort --profile black .
	$(PYTHON) -m black .

natas: $(VENV)/bin/activate
	$(PYTHON) ./natas/run.py

clean:
	rm -rf $(VENV)

.PHONY: lint natas clean
