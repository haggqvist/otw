import base64
import json
import sys
from itertools import cycle
from urllib import parse


def xor(text: str, key: str = "secret") -> str:
    xor_list = zip(text, cycle(key)) if len(text) > len(key) else zip(cycle(text), key)
    return "".join(chr(ord(a) ^ ord(b)) for a, b in xor_list)


def main(cookie: str) -> str:
    url_decoded_cookie = parse.unquote_plus(cookie)
    cookie_bytes = base64.b64decode(url_decoded_cookie).decode()
    key = {"showpassword": "no", "bgcolor": "#ffffff"}
    key = json.dumps(key, separators=(",", ":"))  # compact JSON
    actual_key = xor(cookie_bytes, key=key)[:4]  # assume key size 4
    data = {"showpassword": "yes", "bgcolor": "#ffffff"}
    data = json.dumps(data, separators=(",", ":"))
    new_cookie = xor(data, key=actual_key)
    print(base64.b64encode(new_cookie.encode()).decode())


if __name__ == "__main__":
    main(sys.argv[1])
