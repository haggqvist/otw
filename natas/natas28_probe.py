import base64
import textwrap
from collections import namedtuple
from urllib.parse import unquote

import requests


def main():
    url = "http://natas28.natas.labs.overthewire.org"
    session = requests.Session()
    session.auth = ("natas28", "JW..")

    Query = namedtuple("Query", ["user_query", "query"])
    history: list[Query] = []

    for n in range(0, 50):
        user_query = "a" * n
        with session.post(url, params={"query": user_query}) as response:
            _, query = response.url.split("=")
            query = base64.b64decode(unquote(query)).hex()
            history.append(Query(user_query=user_query, query=query))

    for n, q in enumerate(history):
        print(f"input: {q.user_query} ({len(q.user_query)})")
        print(f"query length: {len(q.query)}")
        if n == 0:
            print(textwrap.fill(q.query, 32))
        else:
            query = textwrap.wrap(q.query, 32)
            prev_query = textwrap.wrap(history[n - 1].query, 32)
            for m, q in enumerate(query):
                print(f"{m}: ", end="")
                if m < len(prev_query):
                    if q == prev_query[m]:
                        print(f" {q}")
                    else:
                        print(f"~{q}")  # changed block
                else:
                    print(f"+{q}")  # new block
        print()


if __name__ == "__main__":
    main()
