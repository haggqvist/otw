from __future__ import annotations

import json
import re
import subprocess
import sys
from dataclasses import dataclass
from pathlib import Path

from rich.progress import (
    MofNCompleteColumn,
    Progress,
    SpinnerColumn,
    TextColumn,
    TimeElapsedColumn,
)

PATTERN = re.compile(r"natas\d+\.(sh|py)")
BASH = "/usr/bin/bash"
CRED_FILE = "natas.json"


@dataclass
class Level:
    file: Path
    name: str
    ext: str
    n: int

    def from_path(p: Path) -> Level:
        file = p
        name = p.stem
        ext = p.suffix
        n = int(name[5:])
        return Level(file, name, ext, n)


cred_file_path = Path(__file__).parent / CRED_FILE
try:
    with open(cred_file_path, "r", encoding="utf-8") as f:
        creds = json.load(f)
except FileNotFoundError:
    creds = {"natas0": "natas0"}


def update_credentials(creds: dict[str, str], cred_file: Path = cred_file_path):
    with open(cred_file, "w", encoding="utf-8") as f:
        f.write(json.dumps(creds, indent=4))


def solve(level: Level, password: str) -> str:
    args = [str(level.file.resolve()), password]
    match level.ext:
        case ".sh":
            args.insert(0, BASH)
        case ".py":
            args.insert(0, sys.executable)
    res = subprocess.run(args, shell=False, capture_output=True)
    out = res.stdout.decode().strip()
    # ignore output other than last line
    if "\n" in out:
        return out.split()[-1]
    return out


def main():
    path = Path(__file__).parent.resolve()
    files = sorted(
        [f for f in path.iterdir() if PATTERN.match(f.name)],
        key=lambda f: int(f.stem[5:]),
    )
    levels = [Level.from_path(f) for f in files]
    progress = Progress(
        SpinnerColumn(),
        TextColumn("[progress.description]{task.description}"),
        MofNCompleteColumn(),
        TimeElapsedColumn(),
    )
    with progress as p:
        p.print("[yellow]Solving natas:")
        t = p.add_task("", total=len(levels))
        for level in levels:
            desc = f"current: \[[green]{level.name}[/green]]"
            if level.n in range(14, 20):
                desc += "\[[red]slow[/red]]"
            p.update(t, description=desc, refresh=True)
            password = creds[level.name]
            next_password = solve(level, password)
            p.advance(t)
            creds[f"natas{level.n+1}"] = next_password
        else:
            p.stop_task(t)
            p.print("[green]Done")
            update_credentials(creds)


if __name__ == "__main__":
    main()
