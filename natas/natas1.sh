NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas1" $NATAS)}"
curl -s http://natas1.natas.labs.overthewire.org -u natas1:$PW |
tac | grep -oEm1 [[:alnum:]]{32}
