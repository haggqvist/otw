NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas8" $NATAS)}"
SECRET=$(python3 $(dirname -- $0)/natas8_pre.py)
curl -s -X POST http://natas8.natas.labs.overthewire.org -u natas8:$PW \
 -F secret=$SECRET -F submit=submit |
tac | grep -oEm1 [[:alnum:]]{32}
