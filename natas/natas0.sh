NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas0" $NATAS)}"
curl -s http://natas0.natas.labs.overthewire.org -u natas0:$PW |
grep -E -o [[:alnum:]]{32}
