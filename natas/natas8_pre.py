import base64

secret = bytes.fromhex("3d3d516343746d4d6d6c315669563362")
secret = secret[::-1]  # reverse
print(base64.b64decode(secret).decode())
