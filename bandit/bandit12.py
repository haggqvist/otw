import sys

from bandit.util import create_connection, get_password

USER = "bandit12"


def main(password: str) -> None:
    with create_connection(USER, password) as c:
        # repeatedly use `file` to find archive type
        tmp = c.run(f"mktemp -d ", hide=True).stdout.strip()
        c.run(f"cp ./data.txt {tmp}"),
        c.run(f"xxd -r {tmp}/data.txt {tmp}/data.gz"),
        c.run(f"zcat {tmp}/data.gz > {tmp}/data"),
        c.run(f"bzcat {tmp}/data > {tmp}/data.gz"),
        c.run(f"zcat {tmp}/data.gz > {tmp}/data.tar"),
        c.run(f"tar -xf {tmp}/data.tar -C {tmp}"),
        c.run(f"tar -xf {tmp}/data5.bin -C {tmp}"),
        c.run(f"bzcat {tmp}/data6.bin > {tmp}/data6.tar"),
        c.run(f"tar -xf {tmp}/data6.tar -C {tmp}"),
        c.run(f"zcat {tmp}/data8.bin > {tmp}/pw.txt"),
        c.run(f"cat {tmp}/pw.txt | grep -oE '[[:alnum:]]{{32}}'"),
        c.run(f"rm -rf {tmp}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
