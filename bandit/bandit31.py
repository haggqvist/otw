import sys

from bandit.util import create_connection, get_password

USER = "bandit31"


def main(password: str) -> None:
    with create_connection(USER, password) as c:
        tmp = c.run(f"mktemp -d ", hide=True).stdout.strip()
        # how to automate through prompts?
        c.run(f"echo 'May I come in? > {tmp}/key.txt")
        c.run("git add -f key.txt")
        c.run("git commit -m 'whatever'")
        c.run(f"git push -u |grep '[[:alnum:]]{{32}}")
        c.run(f"rm -rf {tmp}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
