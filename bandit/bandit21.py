import sys

from bandit.util import get_password, run

USER = "bandit21"


def main(password: str) -> None:
    run(
        user=USER,
        password=password,
        commands=["cat $(cat /usr/bin/cronjob_bandit22.sh | grep -oEm1 '/tmp/.+')"],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
