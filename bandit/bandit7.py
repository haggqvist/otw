import sys

from bandit.util import get_password, run

USER = "bandit7"


def main(password: str) -> None:
    run(
        user=USER,
        password=password,
        commands=["grep millionth data.txt | cut -f2"],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
