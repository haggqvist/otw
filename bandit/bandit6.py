import sys

from bandit.util import get_password, run

USER = "bandit6"


def main(password: str) -> None:
    run(
        user=USER,
        password=password,
        commands=["cat $(find / -group bandit6 -user bandit7 2> /dev/null)"],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
