import sys

from bandit.util import get_password, run

USER = "bandit9"


def main(password: str) -> None:
    run(
        user=USER,
        password=password,
        commands=["strings data.txt | grep -oE '[[:alnum:]]{32}'"],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
