import sys

from bandit.util import get_password, create_connection


USER = "bandit20"


def main(password: str) -> None:
    with create_connection(USER, password) as c:
        tmp = c.run(f"mktemp -d ", hide=True).stdout.strip()
        c.run(f"echo {password} > {tmp}/20.pw")
        c.run(f"echo 'nc -l 31337 < {tmp}/20.pw > {tmp}/20.log' > {tmp}/web.sh")
        c.run(f"chmod +x {tmp}/web.sh")
        c.run(f"screen -dm bash -c {tmp}/web.sh")
        c.run("./suconnect 31337 > /dev/null")
        c.run(f"cat {tmp}/20.log")
        c.run(f"rm -rf {tmp}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
