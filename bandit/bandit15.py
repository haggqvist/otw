import sys

from bandit.util import get_password, run

USER = "bandit15"


def main(password: str) -> None:
    command = (
        f"echo '{password}' |"
        "openssl s_client -connect localhost:30001 -quiet 2> /dev/null |"
        "grep -oE '[[:alnum:]]{32}'"
    )
    run(
        user=USER,
        password=password,
        commands=[command],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
