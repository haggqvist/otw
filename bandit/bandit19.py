import sys

from bandit.util import get_password, run

USER = "bandit19"


def main(password: str) -> None:
    run(
        user=USER,
        password=password,
        commands=["./bandit20-do cat /etc/bandit_pass/bandit20"],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
