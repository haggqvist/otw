from io import StringIO
import json
import sys
from pathlib import Path

from fabric import Connection
from paramiko import RSAKey

HOST = "bandit.labs.overthewire.org"
PORT = 2220
CRED_FILE = "bandit.json"


cred_file_path = Path(__file__).parent / CRED_FILE
try:
    with open(cred_file_path, "r", encoding="utf-8") as f:
        creds = json.load(f)
except FileNotFoundError:
    creds = {"bandit0": "bandit0"}


def update_credentials(creds: dict[str, str], cred_file: Path = cred_file_path):
    with open(cred_file, "w", encoding="utf-8") as f:
        f.write(json.dumps(creds, indent=4))


def get_password(user: str) -> str:
    try:
        return creds[user]
    except KeyError as e:
        print(f"No password supplied and not found in {CRED_FILE}")
        sys.exit(1)


def create_connection(user: str, password: str) -> Connection:
    if len(password) > 32:
        conn_args = {"pkey": RSAKey.from_private_key(StringIO(password))}
    else:
        conn_args = {"password": password}
    return Connection(host=HOST, port=PORT, user=user, connect_kwargs=conn_args)


def run(
    user: str,
    password: str,
    commands: list[str],
) -> None:
    with create_connection(user=user, password=password) as c:
        for command in commands:
            c.run(command)
