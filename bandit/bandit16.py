import sys

from bandit.util import get_password, run

USER = "bandit16"


def main(password: str) -> None:
    # find ports
    # netstat -ltn 2> /dev/null | grep -oE '31[0-9]+'
    # 31518
    # 31790
    command = (
        f"echo '{password}'"
        "| openssl s_client -connect localhost:31790 -quiet 2> /dev/null"
        "| grep -v Correct! | grep -v ^$"
    )
    run(user=USER, password=password, commands=[command])


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
