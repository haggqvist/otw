import sys

from bandit.util import create_connection, get_password

USER = "bandit28"


def main(password: str) -> None:
    with create_connection(USER, password) as c:
        tmp = c.run(f"mktemp -d ", hide=True).stdout.strip()
        # how to automate through prompts?
        c.run(
            f"git show 899ba88df296331cc01f30d022c006775d467f28 | grep 'password' | grep -oE '[[:alnum:]]{{32}}'"
        )
        c.run(f"rm -rf {tmp}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
