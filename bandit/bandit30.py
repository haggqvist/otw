import sys

from bandit.util import create_connection, get_password

USER = "bandit30"


def main(password: str) -> None:
    with create_connection(USER, password) as c:
        tmp = c.run(f"mktemp -d ", hide=True).stdout.strip()
        # how to automate through prompts?
        c.run(f"git show 831aac2e2341f009e40e46392a4f5dd318483019")
        c.run(f"rm -rf {tmp}")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
