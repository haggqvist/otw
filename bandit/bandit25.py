import sys

from bandit.util import get_password, run

USER = "bandit25"


def main(password: str | None) -> None:
    run(
        user=USER,
        password=password,
        commands=["cat bandit26.sshkey"],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
