import sys

from bandit.util import get_password, create_connection

USER = "bandit26"


def main(password: str) -> None:
    with create_connection(USER, password) as c:
        # no idea how to automate...
        # connect from tiny terminal
        # more pagination triggered
        # v # launch vim
        # :set shell=/bin/bash
        # :shell
        # ./bandit27-do cat /etc/bandit_pass/bandit27
        ...


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
