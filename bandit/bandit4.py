import sys

from bandit.util import get_password, run

USER = "bandit4"


def main(password: str) -> None:
    run(
        user=USER,
        password=password,
        commands=['cat $(file -i inhere/* | grep ascii | cut -d ":" -f1)'],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
