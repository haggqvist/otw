import sys

from bandit.util import get_password, run

USER = "bandit22"


def main(password: str) -> None:
    run(
        user=USER,
        password=password,
        commands=["cat /tmp/8ca319486bfbbc3663ea0fbe81326349"],
    )


if __name__ == "__main__":
    if len(sys.argv) > 1:
        password = sys.argv[1]
    else:
        password = get_password(USER)
    main(password)
