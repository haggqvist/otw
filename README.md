# ctf / over the wire

This repository contains scripts, code snippets and files used for wargames at
[Over the Wire][otw]. For related commentary, see [my website][ctf].

Limited effort has been made to make the scripts runnable as a sequence (was
useful when I needed to re-capture passwords). There is almost no error handling
and I have no intention of improving that.

## Dependencies

Some software is needed to run the scripts: 

* `python` (tested with 3.11)
* `make`
* `jq` to read credentials from local file
* `php` for a few natas levels

## Usage

```console
make natas # run all solutions for natas and create local credenrials file
bash natas1.sh # run single file

# python file
. .venv/bin/activate # see Makefile for setup
python natas/natas15.py $(jq -r "natas15.py" natas/natas.json)

# python module (bandit)
. .venv/bin/activate
python -m bandit.bandit10
```

## [Bandit] TODO

Some stuff I may eventually get around to fixing (probably not):

* levels 26-32 are not yet scripted
* no walkthrough has been written
* no runner implemented
* use ssh keys to get passwords

[ctf]: https://w.haggqvist.ninja/ctf
[otw]: https://overthewire.org/wargames/
[bandit]:https://overthewire.org/wargames/bandit/
